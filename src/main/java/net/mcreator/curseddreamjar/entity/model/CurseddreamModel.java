package net.mcreator.curseddreamjar.entity.model;

import software.bernie.geckolib.model.data.EntityModelData;
import software.bernie.geckolib.model.GeoModel;
import software.bernie.geckolib.core.animation.AnimationState;
import software.bernie.geckolib.core.animatable.model.CoreGeoBone;
import software.bernie.geckolib.constant.DataTickets;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.Minecraft;

import net.mcreator.curseddreamjar.entity.CurseddreamEntity;

public class CurseddreamModel extends GeoModel<CurseddreamEntity> {
	@Override
	public ResourceLocation getAnimationResource(CurseddreamEntity entity) {
		return new ResourceLocation("cursed_dreamjar", "animations/dream_cursed.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(CurseddreamEntity entity) {
		return new ResourceLocation("cursed_dreamjar", "geo/dream_cursed.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(CurseddreamEntity entity) {
		return new ResourceLocation("cursed_dreamjar", "textures/entities/" + entity.getTexture() + ".png");
	}

	@Override
	public void setCustomAnimations(CurseddreamEntity animatable, long instanceId, AnimationState animationState) {
		CoreGeoBone head = getAnimationProcessor().getBone("head");
		if (head != null) {
			int unpausedMultiplier = !Minecraft.getInstance().isPaused() ? 1 : 0;
			EntityModelData entityData = (EntityModelData) animationState.getData(DataTickets.ENTITY_MODEL_DATA);
			head.setRotX(entityData.headPitch() * ((float) Math.PI / 180F) * unpausedMultiplier);
			head.setRotY(entityData.netHeadYaw() * ((float) Math.PI / 180F) * unpausedMultiplier);
		}

	}
}
